# VSCode-Extension-Collection
This Repo contains a Collection of useful VSCode-Extensions

merge-requests are welcome!

## Install on Windows

To use the Install-Script on Windows you have to execute the `./install.ps1`.

For some language specific extensions you have to execute `./language.ps1`

**NOTE** to use the included settings you have to move the `settings.json` and the `keybindings.json` manually to your Visual Studio Code Folder.

## Install on Linux

To use the Install-Script on Linux you have to make the `./install.sh` executable.

```bash
chmod +x install.sh
./install.sh
```
For some language specific extensions you have to do the same with the `./language.sh`

```bash
chmod +x language.sh
./language.sh
```
